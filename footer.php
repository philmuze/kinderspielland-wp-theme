			<footer class="footer" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">

				<div id="inner-footer" class="container">

						<div class="row">
						<div class="col-xs-12 col-sm-6 col-md-6">

								<?php if ( is_active_sidebar( 'company_footer' ) ) : ?>
									<?php dynamic_sidebar( 'company_footer' ); ?>
								<?php else : ?>
									<?php
										/*
										 * This content shows up if there are no widgets defined in the backend.
										*/
									?>
									<div class="no-widgets">
										<p><?php _e( 'This is a widget ready area. Add some and they will appear here.', 'ksl' );  ?></p>
									</div>

								<?php endif; ?>


								<ul class="social-icons icon-circle icon-zoom list-unstyled list-inline">
	        	      <li> <a href="https://www.facebook.com/kinderspielland" target="_blank"><i class="fa fa-facebook"></i></a></li>
	        	      <li> <a href="https://twitter.com/KinderSpielLand" target="_blank"><i class="fa fa-twitter"></i></a></li>
	        	  	</ul>
						</div>

						<div class="col-xs-12 col-sm-6">
							<div class="row member">
								<div class="col-xs-12">
									<?php if ( is_active_sidebar( 'logos_footer' ) ) : ?>
										<?php dynamic_sidebar( 'logos_footer' ); ?>
									<?php else : ?>
										<?php
											/*
											 * This content shows up if there are no widgets defined in the backend.
											*/
										?>
										<div class="no-widgets">
											<p><?php _e( 'This is a widget ready area. Add some and they will appear here.', 'ksl' );  ?></p>
										</div>

									<?php endif; ?>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12 col-sm-4 col-md-4 nav-footer">
									<h4>Partner:</h4>

									<nav role="navigation">
										<?php wp_nav_menu(array(
				    					'container' => 'div',                           // enter '' to remove nav container (just make sure .footer-links in _base.scss isn't wrapping)
				    					'container_class' => 'footer-links cf',         // class of container (should you choose to use it)
				    					'menu' => __( 'Footer Partner', 'ksl' ),   // nav name
				    					'menu_class' => 'nav footer-nav cf',            // adding custom nav class
				    					'theme_location' => 'footer-partner',             // where it's located in the theme
				    					'before' => '',                                 // before the menu
				    					'after' => '',                                  // after the menu
				    					'link_before' => '',                            // before each link
				    					'link_after' => '',                             // after each link
				    					'depth' => 0,                                   // limit the depth of the nav
				    					'fallback_cb' => 'bones_footer_links_fallback'  // fallback function
										)); ?>
									</nav>
								</div>

								<div class="col-xs-12 col-sm-4 col-md-4 nav-footer">
									<h4>Weiteres:</h4>
									<nav role="navigation">
										<?php wp_nav_menu(array(
				    					'container' => 'div',                           // enter '' to remove nav container (just make sure .footer-links in _base.scss isn't wrapping)
				    					'container_class' => 'footer-links cf',         // class of container (should you choose to use it)
				    					'menu' => __( 'Footer Links', 'ksl' ),   // nav name
				    					'menu_class' => 'nav footer-nav cf',            // adding custom nav class
				    					'theme_location' => 'footer-links',             // where it's located in the theme
				    					'before' => '',                                 // before the menu
				    					'after' => '',                                  // after the menu
				    					'link_before' => '',                            // before each link
				    					'link_after' => '',                             // after each link
				    					'depth' => 0,                                   // limit the depth of the nav
				    					'fallback_cb' => 'bones_footer_links_fallback'  // fallback function
										)); ?>
									</nav>
								</div>

								<div class="col-xs-12 col-sm-4 col-md-4 nav-footer">
									<h4>Rechtliches:</h4>
									<nav role="navigation">
										<?php wp_nav_menu(array(
											'container' => 'div',                           // enter '' to remove nav container (just make sure .footer-links in _base.scss isn't wrapping)
											'container_class' => 'footer-links cf',         // class of container (should you choose to use it)
											'menu' => __( 'Footer Rechtliches', 'ksl' ),   // nav name
											'menu_class' => 'nav footer-nav cf',            // adding custom nav class
											'theme_location' => 'footer-rechtliches',             // where it's located in the theme
											'before' => '',                                 // before the menu
											'after' => '',                                  // after the menu
											'link_before' => '',                            // before each link
											'link_after' => '',                             // after each link
											'depth' => 0,                                   // limit the depth of the nav
											'fallback_cb' => 'bones_footer_links_fallback'  // fallback function
										)); ?>
									</nav>
								</div>

					</div>
					</div>
					</div>

					<p class="source-org copyright">&copy; <?php echo date('Y'); ?> <?php bloginfo( 'name' ); ?> </p>
				</div>
			</footer>
		</div>


		<script src="<?php echo get_template_directory_uri(); ?>/bower_components/bootstrap/dist/js/bootstrap.min.js"></script><!-- .bootstrap script -->		



		<?php // all js scripts are loaded in library/bones.php ?>
		<?php wp_footer(); ?>

	</body>

</html> <!-- end of site. what a ride! -->
