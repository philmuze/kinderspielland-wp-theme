var gulp = require('gulp'),
  sass = require('gulp-sass'),
  browserSync = require('browser-sync').create();

gulp.task('sass', function() {
  return gulp.src('./library/scss/**/*.scss')
    .pipe(sass({
      'outputStyle': 'expanded'
    }))
    .pipe(gulp.dest('./library/css'))
    .pipe(browserSync.stream());
});



gulp.task('serve', ['sass'], function() {
  browserSync.init({
    proxy: "http://ksl-wp.dev"
  });

  gulp.watch('./library/scss/**/*.scss', ['sass']);
  gulp.watch('./**/*.php').on('change', browserSync.reload);
});


gulp.task('default', ['serve']);
