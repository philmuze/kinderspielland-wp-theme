<?php
/*
 Template Name: Startseite
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>

<?php get_header(); ?>

			<div id="content">

				<div id="inner-content" class="container">

				<div class="row">
					<div class="hero-header col-md-12">
						<div class="schild-top">
							<h2><?php _e('Der Indoorspielplatz für jede Jahreszeit','ksl') ?></h2>
							<div class="opening">
								<?php // Use shortcode in a PHP file (outside the post editor).
	                      echo do_shortcode('[iva_bhrs name="tag" single_day="on"]');?>
							</div>
						</div>

					</div>
				</div>

				<div class="row section-newsstream">
					<div class="col-md-12">
						<h2 class="schild-bg"><?php _e('Aktuelles aus Görlitz', 'ksl') ?></h2>
						<div class="facebook-content">
							<section class="news-frontpage">
								<div class="row">
									<div class="col-xs-12 col-md-8">
										<?php $args = array('post_type' => 'post', 'showposts' => 2, 'orderby' => 'rand');
			                                $loop = new WP_Query($args);
			                                while ($loop->have_posts()) : $loop->the_post();
			                                ?>

													<h3><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
													<p><?php the_content(); ?></p>
													<hr>
											<?php endwhile; ?>


									</div>
									<div class="col-xs-12 col-md-4">
										<?php // Use shortcode in a PHP file (outside the post editor).
				              echo do_shortcode('[fts_facebook id=kinderspielland posts_displayed=page_only posts=2 title=no description=no words=25 type=page hide_like_option=no]');?>
										</div>
									</div>
								</div>
							</section>
					</div>
				</div>
			</div>



			<div class="white-bg">
				<div class="container section-bildergalerie">
				<div class="row">
					<div class="negative-top-margin">
						<div class="col-xs-12 col-sm-3 col-md-3">
							<img class="img-responsive w-200" src="<?php echo get_template_directory_uri(); ?>/library/images/kind-kart.png">
						</div>
						<div class="col-xs-12 col-sm-6 col-md-6">
							<h2 class="schild-bg"><?php _e('Eindrücke und Bilder','ksl')?></h2>
						</div>
						<div class="hidden-xs col-sm-3 col-md-3 text-center">
							<img class="img-responsive w-200" src="<?php echo get_template_directory_uri(); ?>/library/images/kind-rutsche.png">
						</div>
					</div>
				</div>
					<div class="row gallery">
						 <?php masterslider(2); ?>
					 </div>
				</div>
		 </div

		 <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		 <div class="section-offers">
			 <div class="container">
			 <div class="row offer-header">
 				<div class="negative-top-margin">
 					<div class="col-xs-3 col-sm-3 col-md-3">
 					</div>
 					<div class="col-xs-6 col-sm-6 col-md-6">
 						<h2 class="schild-bg"><?php _e('Angebote und Aktionen','ksl')?></h2>
 					</div>
 					<div class="col-xs-3 col-sm-3 col-md-3">
 					</div>
 				</div>
 			</div>
			<div class="row offer">
				<div class="col-xs-12 white-bg blue-border center">
					<a href="<?php the_field( "link_fur_aktion" ); ?>" title="Unser Angebot für Kindergeburtstage">

						<img class="img-responsive centered" src="<?php the_field('aktionsbild_1'); ?>"></a>
					<a class="btn btn-primary" href="<?php the_field( "link_fur_aktion" ); ?>"><?php the_field( "button_aktion_1" ); ?></a>
				</div>
			</div>

			<div class="row offer">
				<div class="col-xs-12 white-bg blue-border center">
					<a href="/kindergeburtstag" title="Unser Angebot für Kindergeburtstage"><img class="img-responsive centered" src="<?php the_field('aktionsbild_2'); ?>"></a>
				 	<a class="btn btn-primary" href="/kindergeburtstag/"><?php the_field( "button_aktion_2" ); ?></a>
				</div>
			</div>
		</div>
		</div>

	<?php endwhile; ?>
	<?php else : ?>
	<?php endif; ?>


	</div>

<?php get_footer(); ?>
