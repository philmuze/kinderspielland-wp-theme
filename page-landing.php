<?php
/*
 Template Name: Landing Page
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>

<!doctype html>
	<head>
		<meta charset="utf-8">

		<?php // force Internet Explorer to use the latest rendering engine available ?>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<title><?php the_field("page_title"); ?></title>

		<?php // mobile meta (hooray!) ?>
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<meta name="viewport" content="width=device-width, initial-scale=1"/>

		<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/library/css/landing-page.css">
	</head>
	<body <?php body_class(); ?> itemscope itemtype="http://schema.org/WebPage">

		<div class="landing-page_wrapper">
			<div class="landing-page_image">
			<?php 
				$image = get_field('page_image');
				if( !empty($image) ): ?>

					<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

			<?php endif; ?>
			</div>
			<div class="landing-page_text">
				<?php 
					$text = get_field('page_text');
					if(!empty($text)): 
					echo $text; 
					endif;
				 ?>
			</div>
		</div>
		
	</body>
</html> <!-- end of site. what a ride! -->