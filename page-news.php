<?php
/*
 Template Name: Blog-Template
 *
 * This is your custom page template. You can create as many of these as you need.
 * Simply name is "page-whatever.php" and in add the "Template Name" title at the
 * top, the same way it is here.
 *
 * When you create your page, you can just select the template and viola, you have
 * a custom page template to call your very own. Your mother would be so proud.
 *
 * For more info: http://codex.wordpress.org/Page_Templates
*/
?>

<?php get_header(); ?>

			<div id="content">

				<div id="inner-content" class="container">

					<main id="main" class="col-md-9" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class('cf'); ?> role="article">

						<header class="article-header">

							<h1 class="h2 entry-title"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h1>

						</header>

						<?php endwhile; ?>
						<?php else : ?>
						<?php endif; ?>

						<section class="entry-content cf">
							<?php $args = array('post_type' => 'post', 'showposts' => 2, 'orderby' => 'rand');
                                $loop = new WP_Query($args);
                                while ($loop->have_posts()) : $loop->the_post();
                                ?>

										<h3><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
										<p><?php the_content(); ?></p>
								<?php endwhile; ?>

							<hr>
							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
							<?php the_content(); ?>
						</section>

					</article>

					<?php endwhile; ?>

							<?php bones_page_navi(); ?>

					<?php else : ?>

							<article id="post-not-found" class="hentry cf">
									<header class="article-header">
										<h1><?php _e('Oops, Post Not Found!', 'ksl'); ?></h1>
								</header>
									<section class="entry-content">
										<p><?php _e('Uh Oh. Something is missing. Try double checking things.', 'ksl'); ?></p>
								</section>
								<footer class="article-footer">
										<p><?php _e('This is the error message in the index.php template.', 'ksl'); ?></p>
								</footer>
							</article>

					<?php endif; ?>

										</main>

						<?php get_sidebar(); ?>

				</div>

			</div>

<?php get_footer(); ?>
